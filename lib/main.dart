import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
        home: Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text("dice"),
        backgroundColor: Colors.black,
        centerTitle: true,
      ),
      body: DicePage(),
    )));

class DicePage extends StatefulWidget {
  @override
  DicePageState createState() => DicePageState();
}

class DicePageState extends State<DicePage> {
  // This widget is the root of your application.
  void DiceFace(){
    setState(() {
      LeftDice = Random().nextInt(6) + 1;
      Rightdice = Random().nextInt(6) + 1;
    });
  }
  int LeftDice = 5;
  int Rightdice = 6;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: FlatButton(
                onPressed: () {
                  setState(() {
                    DiceFace();


                  });
                },
                child: Image.asset("images/dice$LeftDice.png"),
              ),
            ),
          ),

          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: FlatButton(
                onPressed: () {
                  setState(() {
                    DiceFace();                  });
                },
                child: Image.asset("images/dice$Rightdice.png"),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
